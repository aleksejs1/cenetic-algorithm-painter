// Genetic algorithm painter
// Created by: A.Kovalovs
// All rights reserved

// ----------------- Global variables ----------------- //
var gLength = 0;
var gPop = 0;
var gGen = 0;
var dd = 1;
var s = "";
var gPSt = 0.95;
var gSop = 0.4;
var gSbp = 0.5;


// ----------------- Functions ----------------- //

// function for making empty field on screen
function makeField(length, index){
	gLength = length;
	var new_string = "";
	for(var i = 0;i<length;i++){
		for(var j = 0;j<length;j++){
			new_string = new_string + '<div id="element_'+index+'_y_'+i+'_x_'+j+'" onmousedown="makeRed('+j+','+i+',\''+index+'\');" class="empty"></div>';
		}
	}
	$("#"+index+"_field").css("width",length+"em");
	$("#"+index+"_field").css("height",length+"em");
	$("#"+index+"_field").html(new_string);
}

// function for inverting color of pixel
function makeRed(x,y,index){
	var element = '#element_'+index+'_y_'+y+'_x_'+x;
	if($(element).hasClass("red")) $(element).removeClass("red");
	else $(element).addClass("red");
}

// function returns an array string from screen value
function getArray(index){
	var s = "arr = [\n";
	for(var i = 0;i<gLength;i++){
		s = s + "       ["
		for(var j = 0;j<gLength;j++){
			if($('#element_'+index+'_y_'+i+'_x_'+j).hasClass("red")) s = s + "'r',";
			else s = s + "'e',";
		}
		s = s.substring(0,s.length - 1) + "],\n";
	}
	s = s.substring(0,s.length - 2) + "\n      ];";
	return s;
}

// function load picture to screen from array
function loadPicture(arr,index) {
	gLength = arr.length;
	for(var i = 0;i<gLength;i++){
		for(var j = 0;j<gLength;j++){
			element = '#element_'+index+'_y_'+i+'_x_'+j;
			if(arr[i][j]=="e") $(element).removeClass("red");
			else if(arr[i][j]=="r") $(element).addClass("red");		
		}
	}	
}

// function returns a chromosome string from screen value
function getChromosome(index){
	var s = "";
	for(var i = 0;i<gLength;i++){
		for(var j = 0;j<gLength;j++){
			if($('#element_'+index+'_y_'+i+'_x_'+j).hasClass("red")) s = s + "r";
			else s = s + "e";
		}
	}
	return s;
}

// function load picture to screen from chromosome
function loadChromosome(chrom,index) {
	for(var i = 0;i<gLength;i++){
		for(var j = 0;j<gLength;j++){
			element = '#element_'+index+'_y_'+i+'_x_'+j;
			if(chrom[i*gLength+j]=="e") $(element).removeClass("red");
			else if(chrom[i*gLength+j]=="r") $(element).addClass("red");		
		}
	}	
}

// function returns random element from array
function getRandElement(arr){
	return arr[Math.floor(Math.random()*(arr.length))];
}

// function returns a random generated chromosome
function getRandChromosome() {
	var s = "";
	for(var i = 0;i<gLength*gLength;i++){
		s = s + getRandElement(["r","e"]);
	}
	return s;
}

// function compare two chromosomes and return equal pixels count
function fitness(c1,c2) {
	var r = 0;
	for(var i = 0;i<gLength*gLength;i++){
		if(c1[i]==c2[i]) r++;
	}
	return r;
}

// function returns an array or random generated chromosomes
function generateRandPopulation(num){
	var a = new Array;
	for(var i = 0;i<num;i++){
		a[i]=getRandChromosome();
	}
	return a;
}

// function returns an id of max value from array
function getMax(arr){
	var bestnr = 0;
	var bestres = 0;
	for(var i = 0;i<arr.length;i++){
		if(arr[i] > bestres){
			bestnr = i;
			bestres = arr[i];
		}
	}
	return bestnr;
}

// function returns an array of bests chromosomes
function getBests(arr,num){
	var a = new Array;
	var res = new Array;
	for(var i = 0;i<arr.length;i++){
		a[i]=fitness(s,arr[i]);
	}
	for(var i = 0;i<num;i++){
		res[i]=arr[getMax(a)];
		a[getMax(a)] = 0;
	}
	return res;
}

// functuin returns a new chromosome, generated from 2 old chromosomes
function getBaby(c1,c2){
	var baby = "";
	var ran = 0;
	var ralen = 0;
	// for(var i = 0;i<gLength/2;i++){
	// 	ran = Math.floor(Math.random()*gLength*gLength);
	// 	ralen = Math.floor(Math.random()*gLength);
	// 	baby = baby + c1.substring(ran,ran + gLength);
	// 	baby = baby + c2.substring(ran,ran + gLength);	
	// }
	ran = Math.floor(Math.random()*gLength*gLength);
	ralen = Math.floor(Math.random()*gLength);
	baby = c1.substring(0,ran) + c2.substring(ran,ran+ralen) + c1.substring(ran+ralen,c1.length);
	return baby;
}

// function returns an arry of new chromosomes, generated from random old chrmosomes
function getNewPop(arr,num){
	var res = new Array;
	for(var i = 0;i<num;i++){
		res[i]=getBaby(arr[Math.floor(Math.random()*arr.length)],arr[Math.floor(Math.random()*arr.length)]);
	}
	return res;
}

// anotren new population generator
function getNewPop2(arr,best,num){
	var res = new Array;
	for(var i = 0;i<num;i++){
		res[i]=getBaby(best[Math.floor(Math.random()*best.length)],arr[Math.floor(Math.random()*arr.length)]);
	}
	return res;
}

// shows compare
function showCompart() {
	$('#compare').html("Compare result:<br />"+fitness(getChromosome('main'),getChromosome('second')));
	$('#compare').show();
}

// genetic algorithm
function runAlg(populationSize,generation,dravtoc) {
	$("#rst_btn").html("Restars");
	custSet();
	goog = gLength*gLength*gPSt;
	var bsts = 0;
	var bstc = 0;
	var bst_res = "";
	$("#drwc").html("");

	// making first random population
	var pop = generateRandPopulation(populationSize);
	var bst = getBests(pop,1);

	// getting parts sizes
	var oldPopulationSize = Math.floor(gSop*populationSize);
	var bestPopulationSize = Math.floor(gSbp*populationSize);
	var randPopulationSize = populationSize - oldPopulationSize - bestPopulationSize;
	
	var drav = getBests(pop,1);
	// generations loop
	var j = 0;
	while(j<generation){
		pop=doStep(pop,oldPopulationSize,bestPopulationSize,randPopulationSize);
		drav = getBests(pop,1);

		
		if(fitness(s,drav[0]) > bsts){
			if(bstc != 0){
				bst_res = bst_res+"f = "+bsts+" for "+bstc+" steps"+"<br />";
			}
			console.log("aa"+bsts);
			bsts = fitness(s,drav[0]);
			bstc = 0;
			if(dravtoc){
				tmp = $("#drwc").append("<canvas id='c_field_"+j+"'>Canvas</canvas> ");
				drawCanvasField(gLength,drav[0],"c_field_"+j);
			}
		}
		bstc++;
		j++;
		if(fitness(s,drav[0]) >= goog){
			bst_res = bst_res+"f = "+bsts+" for "+bstc+" steps"+"<br />Found!";
			j = 999999;
		}
	}
	if(j!=999999){
		bst_res = bst_res+"f = "+bsts+" for "+bstc+" steps"+"<br />Not found!";
	}
	
	$("#logg").html(bst_res);
	console.log(bst_res);
	$("#logg").show();
	// for(var i = 0;i<generation;i++){
	// 	pop=doStep(pop,oldPopulationSize,bestPopulationSize,randPopulationSize);
	// 	drav = getBests(pop,1);
	// 	if(dravtoc){
	// 		drawCanvasField(gLength,drav[0],"c_field_"+i);
	// 	}
	// }
	return getBests(pop,1)[0];
}

function restartAng() {
	s=getChromosome('main');
	loadChromosome(runAlg(gPop,gGen,1),'second');
	showCompart();
}

function drawCanvasField(len,chr,can1) {
	var canv = document.getElementById(can1), ctx = canv.getContext('2d');
	var siz = 80;
    var kof = siz / len;
    canv.width  = siz + kof + kof+ kof;
    canv.height = siz;
    for(var i = 0;i<gLength;i++){
		for(var j = 0;j<gLength;j++){
			if(chr[i*gLength+j] == "e"){
				ctx.strokeRect(j * kof, i * kof, kof, kof);
			}else{
				ctx.fillRect(j * kof, i * kof, kof, kof);
			}
		}
	}
	ctx.font = "normal 12px sans-serif";
	ctx.fillText("Nr.", gLength * kof +2, 10);	
	ctx.font = "bold 12px sans-serif";
	ctx.fillText(can1.substring(8), gLength * kof + 2, 20);	
	ctx.font = "normal 12px sans-serif";
	ctx.fillText("Fitn-", gLength * kof +2, 40);	
	ctx.fillText("ess:", gLength * kof +2, 50);	
	ctx.font = "bold 12px sans-serif";
	ctx.fillText(fitness(s,chr), gLength * kof + 2, 60);	
}

function doStep(pop,oldPopulationSize,bestPopulationSize,randPopulationSize){		
	var tt = getBests(pop,1);
	console.log(fitness(s,tt[0]));
	bst = getBests(pop,bestPopulationSize);
	pop=getBests(pop,oldPopulationSize);
	var tmp=getNewPop2(bst,pop,bestPopulationSize);
	for(var j = 1;j<bestPopulationSize+1;j++){
		pop.push(tmp[j-1]);
	}
	tmp=generateRandPopulation(randPopulationSize);
	for(var j = 1;j<randPopulationSize+1;j++){
		pop.push(tmp[j-1]);
	}
	return pop;
	// drav = getBests(pop,1);
	// drawCanvasField(gLength,drav[0],"c_field_"+i);
}

function custSet(){
	gLength = $("#sz").val();
	gPop = $("#pp").val();
	gGen = $("#ge").val();
	gPSt = $("#ss").val();
	gSop = $("#sop").val();
	gSbp = $("#sbp").val();
	makeField(gLength,"main");
	if(gLength == 10){
		s = "errreerereeereerrrrreereeerrreerrreeereeeeeeeeeeeerrreerrereereereeereereeereereereeeerereereerreere";
	}else if(gLength == 9){
		s = "reeeeeeerereeeeereeereeereeeeerereeeeeeereeeeeeerereeeeereeereeereeeeerereeeeeeer";
	}else if(gLength == 8){
		s = "reeeeeerereeeereeereereeeeerreeeeeerreeeeereereeereeeerereeeeeer";
	}else if(gLength == 7){
		s = "reeeeerereeereeerereeeeereeeeerereeereeerereeeeer";
	}else if(gLength == 6){
		s = "reeeerereereeerreeeerreeereerereeeer";
	}else if(gLength == 5){
		s = "reeererereeereeererereeer";
	}else if(gLength == 4){
		s = "reererreerrereer";
	}else if(gLength == 3){
		s = "rerererer";
	}else if(gLength == 2){
		s = "reer";
	}
	// s = "reeererereeereeererereeer"; //x
	loadChromosome(s,'main');
	makeField(gLength,"second");
}


// ----------------- Program ----------------- //

// Settings
gLength = 5;
gPop = 50;
gGen = 100;
gPSt = 0.95;
gSop = 0.4;
gSbp = 0.5;

$("#sz").val(gLength);
$("#pp").val(gPop);
$("#ge").val(gGen);
$("#ss").val(gPSt);
$("#sop").val(gSop);
$("#sbp").val(gSbp);
